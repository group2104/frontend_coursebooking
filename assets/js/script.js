//Add the logic that will show the login button when the user is not logged in


let navItems = document.querySelector("#navSession");
console.log(navItems);

let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {
    navItems.innerHTML = 
    `
        <li class="nav-item">
            <a href="./login.html" class="nav-link">Log in</a>
        </li>
    `
}



